// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "IndicatorsAttributeSet.generated.h"

/**
 * 
 */
// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

UCLASS()
class UE5CPPROBOTDREAMS_API UIndicatorsAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category="Health")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UIndicatorsAttributeSet, Health)

	UPROPERTY(BlueprintReadOnly, Category="Health")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UIndicatorsAttributeSet, MaxHealth)

	UPROPERTY(BlueprintReadOnly, Category="Mana")
	FGameplayAttributeData Mana;
	ATTRIBUTE_ACCESSORS(UIndicatorsAttributeSet, Mana)

	UPROPERTY(BlueprintReadOnly, Category="Mana")
	FGameplayAttributeData MaxMana;
	ATTRIBUTE_ACCESSORS(UIndicatorsAttributeSet, MaxMana)

	UPROPERTY(BlueprintReadOnly, Category="Stamina")
	FGameplayAttributeData Stamina;
	ATTRIBUTE_ACCESSORS(UIndicatorsAttributeSet, Stamina)
	
	UPROPERTY(BlueprintReadOnly, Category="Stamina")
	FGameplayAttributeData MaxStamina;
	ATTRIBUTE_ACCESSORS(UIndicatorsAttributeSet, MaxStamina)
	
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;
};
