// Fill out your copyright notice in the Description page of Project Settings.


#include "IndicatorsAttributeSet.h"

void UIndicatorsAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
		Super::PreAttributeChange(Attribute, NewValue);
	
		if(GetHealthAttribute() == Attribute)
		{
			NewValue = FMath::Clamp(NewValue, 0, GetMaxHealth());
		}
		if(GetManaAttribute() == Attribute)
		{
			NewValue = FMath::Clamp(NewValue, 0, GetMaxMana());
		}
	if(GetStaminaAttribute() == Attribute)
	{
		NewValue = FMath::Clamp(NewValue, 0, GetMaxStamina());
	}
}
void UIndicatorsAttributeSet::PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue)
{
		Super::PostAttributeChange(Attribute, OldValue, NewValue);

		if(GetHealthAttribute() == Attribute)
		{
			if(NewValue <= 0)
			{
				/*...*/
			}
		}
}