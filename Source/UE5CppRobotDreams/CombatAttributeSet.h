// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "AttributeSet.h"
#include "CombatAttributeSet.generated.h"

/**
 * 
 */
// Uses macros from AttributeSet.h
#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
		GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

UCLASS()
class UE5CPPROBOTDREAMS_API UCombatAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadOnly, Category="FireDamage")
	FGameplayAttributeData FireDamage;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, FireDamage)
	
	UPROPERTY(BlueprintReadOnly, Category="FireDamage")
	FGameplayAttributeData FireDamageMultiplier;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, FireDamageMultiplier)

	UPROPERTY(BlueprintReadOnly, Category="WaterDamage")
	FGameplayAttributeData WaterDamage;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, WaterDamage)
	
	UPROPERTY(BlueprintReadOnly, Category="WaterDamage")
	FGameplayAttributeData WaterDamageMultiplier;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, WaterDamageMultiplier)

	UPROPERTY(BlueprintReadOnly, Category="LightningDamage")
	FGameplayAttributeData LightningDamage;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, LightningDamage)
	
	UPROPERTY(BlueprintReadOnly, Category="LightningDamage")
	FGameplayAttributeData LightningDamageMultiplier;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, LightningDamageMultiplier)
	
	UPROPERTY(BlueprintReadOnly, Category="WindDamage")
    FGameplayAttributeData WindDamage;
    ATTRIBUTE_ACCESSORS(UCombatAttributeSet, WindDamage)
    
    UPROPERTY(BlueprintReadOnly, Category="WindDamage")
    FGameplayAttributeData WindDamageMultiplier;
    ATTRIBUTE_ACCESSORS(UCombatAttributeSet, WindDamageMultiplier)

	UPROPERTY(BlueprintReadOnly, Category="PureDamage")
	FGameplayAttributeData PureDamage;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, PureDamage)
    
	UPROPERTY(BlueprintReadOnly, Category="PureDamage")
	FGameplayAttributeData PureDamageMultiplier;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, PureDamageMultiplier)
	
	UPROPERTY(BlueprintReadOnly, Category="WeaponDamage")
	FGameplayAttributeData WeaponDamage;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, WeaponDamage)
    
	UPROPERTY(BlueprintReadOnly, Category="WeaponDamage")
	FGameplayAttributeData WeaponDamageMultiplier;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, WeaponDamageMultiplier)
	
	UPROPERTY(BlueprintReadOnly, Category="Heal")
	FGameplayAttributeData Heal;
	ATTRIBUTE_ACCESSORS(UCombatAttributeSet, Heal)
};
